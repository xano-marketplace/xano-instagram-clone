import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class AuthService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public login(login: { email: string, password: string }): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/login`,
			params: {
				email: login.email,
				password: login.password
			}
		});
	}

	public signup(signup: { name: string, email: string, handle: string, password: string }): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/signup`,
			params: signup
		});
	}

	public me(): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/auth/me`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}
}
