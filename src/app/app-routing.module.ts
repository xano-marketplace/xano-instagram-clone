import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {ProfileComponent} from "./profile/profile.component";
import {AuthGuard} from "./auth.guard";
import {DiscoverComponent} from "./discover/discover.component";

const routes: Routes = [
	{path: '', component: HomeComponent},
	{path: 'discover', component: DiscoverComponent,  canActivate: [AuthGuard]},
	{path: 'discover/:tag', component: DiscoverComponent,  canActivate: [AuthGuard]},
	{path: 'profile/:user_id', component: ProfileComponent, canActivate: [AuthGuard]}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {
}
