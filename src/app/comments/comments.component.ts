import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';
import {finalize} from "rxjs/operators";
import {FormControl, Validators} from "@angular/forms";
import {ConfigService} from "../config.service";
import {PhotoService} from "../photo.service";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-comments',
	templateUrl: './comments.component.html',
	styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {


	public commentInput: FormControl = new FormControl('', Validators.required);
	public comments: any = [];
	public hasNextPage: boolean;
	public currentPage: number = 1;
	public user: any;
	@Input('photoID') photoID: number;
	@Input('photoOwnerID') photoOwnerID: number;

	constructor(
		private photoService: PhotoService,
		private configService: ConfigService,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.configService.user.asObservable().subscribe(res => {
			this.user = res
		},error =>  {
			this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
				panelClass: 'error-snack'
			})
		});
		setTimeout(() => {
			this.photoService.commentsGet(this.photoID, null).subscribe(res => {
				this.comments = res['items'];
				this.currentPage = res["curPage"];
				this.hasNextPage = !!res["nextPage"];
			});
		}, 1000);
	}

	public postComment() {
		if (this.commentInput.value) {
			this.photoService.commentSave({
				comment: this.commentInput.value,
				photo_id: this.photoID
			}).toPromise().then(res => {
				this.comments.unshift(res);
				this.commentInput.patchValue('');
			}).catch(error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}

	public allowDelete(comment): boolean {
		return this.user?.id && (comment.user_id === this.user?.id || this.photoOwnerID === this.user?.id);
	}

	public deleteComment(comment) {
		this.photoService.commentDelete(comment.id).pipe(
			finalize(() => this.comments = this.comments.filter(x => x.id !== comment.id))
		).subscribe(res => {

		},error =>  {
			this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
				panelClass: 'error-snack'
			})
		});
	}

	public loadMore() {
		if (this.hasNextPage) {
			this.photoService.commentsGet(this.photoID, {page: this.currentPage + 1}).subscribe(res => {
				if (res["itemsReceived"] > 0) {
					this.currentPage = res["curPage"];
					this.hasNextPage = !!res["nextPage"]
					this.comments = [...this.comments, ...res["items"]];
				}
			},error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}

}
