import {Component, Inject, OnInit} from '@angular/core';
import {PhotoService} from "../photo.service";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
	selector: 'app-like-list-panel',
	templateUrl: './like-list-panel.component.html',
	styleUrls: ['./like-list-panel.component.scss']
})
export class LikeListPanelComponent implements OnInit {
    public likes: any = [];

	constructor(
	    private photoService: PhotoService,
        @Inject(MAT_DIALOG_DATA) public data
    ) {
	}

	ngOnInit(): void {
	    if(this.data.photo_id) {
            this.photoService.likesGet(this.data.photo_id, null).subscribe(res => {
               this.likes = res.items;
            })
        }
	}

}
