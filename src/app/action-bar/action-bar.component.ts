import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {debounceTime, filter, finalize, map, mergeMap, switchMap, tap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ConfigPanelComponent} from '../config-panel/config-panel.component';
import {AuthPanelComponent} from '../auth-panel/auth-panel.component';
import {ManagePanelComponent} from '../manage-panel/manage-panel.component';
import {FormControl} from "@angular/forms";
import {UserService} from "../user.service";
import {PhotoService} from "../photo.service";

@Component({
	selector: 'app-action-bar',
	templateUrl: './action-bar.component.html',
	styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {

	public isLoggedIn: boolean = false;
	public isConfigured: boolean = false;
	public isHome: boolean = true;
	public discoverPage: boolean;
	public config: XanoConfig;
	public user: any;
	public isLoading: boolean = false;

	public searchControl  = new FormControl('');
	public searchResults: any;

	constructor(
		private configService: ConfigService,
		private userService: UserService,
		private photoService: PhotoService,
		private router: Router,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res => this.isConfigured = res);
		this.config = this.configService.config;

		this.searchControl.valueChanges.pipe(
			debounceTime(300),
			tap(() => {
				this.searchResults = [];
				this.isLoading = true;
			}),
			switchMap(value => {
				return this.userService.usersGet(this.constructSearch(value))
					.pipe(
						finalize(() => {
							this.isLoading = false
						}),
					)
				}
			)
		).subscribe(data => {
			this.searchResults = data?.items;
		});

		this.configService.isLoggedIn()
			.pipe(mergeMap(loggedIn => {
				this.isLoggedIn = loggedIn;
				if (loggedIn) {
					return this.configService.user.asObservable();
				} else {
					return EMPTY;
				}
			}))
			.subscribe(user => {
				this.user = user;
			});

		this.router.events.pipe(
			filter((event: RouterEvent) => event instanceof NavigationEnd),
			map(event => event.url)
		).subscribe(url => {
			this.discoverPage = url === '/discover'
			this.isHome = url === '/'
		});

		this.configService.user.asObservable().subscribe(res => {
			this.user = res;
		})

	}

	public logout(): void {
		this.configService.authToken.next(null);
		this.configService.user.next(null);
	}

	public showPanel(dialogType): void {
		let dialogRef;
		switch (dialogType) {
			case 'config':
				dialogRef = this.dialog.open(ConfigPanelComponent);
				break;
			case 'auth':
				dialogRef = this.dialog.open(AuthPanelComponent);
				break;
			case 'manage':
				dialogRef = this.dialog.open(ManagePanelComponent);
				dialogRef.afterClosed().subscribe(res => {
					if(res?.new) {
						this.configService.newPhoto.next(res.item);
					}
				})
				break;
			default:
				break;
		}
	}

	private constructSearch(search): any {
		const searchValue = search.trim();

		if (searchValue) {
			return {
				expression: [{
					'type': 'group',
					'group': {
						'expression': [
							{
								'statement': {
									'left': {
										'tag': 'col',
										'operand': 'user.handle'
									},
									'op': 'includes',
									'right': {
										'operand': `${searchValue}`
									}
								}
							},
							{
								'or': true,
								'statement': {
									'left': {
										'tag': 'col',
										'operand': 'user.name'
									},
									'op': 'includes',
									'right': {
										'operand': `${searchValue}`
									}
								}
							},
						]
					}
				}]
			};
		} else {
			return [];
		}
	}
}
