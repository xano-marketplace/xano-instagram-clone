import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class UserService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public userGet(userID): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/user/${userID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public usersGet(search): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/user`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: { search }
		});
	}

	public userPhotosGet(userID) {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/user/${userID}/photos`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public userFollow(userID): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/follows`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: {followed_id: userID}
		});
	}

	public userUnfollow(userID): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/follows/${userID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`}
		});
	}

	public userFollowsGet(userID, following): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/follows/${userID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: { following: following }
		});
	}


}
