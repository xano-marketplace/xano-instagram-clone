import {Injectable} from '@angular/core';
import {ApiService} from "./api.service";
import {ConfigService} from "./config.service";
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class PhotoService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public commentsGet(photoID, external_paging) {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/comments/${photoID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: { external_paging }
		});
	}

	public commentSave(comment) {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/comment`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: comment
		})
	}

	public commentDelete(commentID) {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/comment/${commentID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public photosGet(search) {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/photo`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: { search }
		});
	}

	public photoGet(photoID) {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/photo/${photoID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public photoUpload(image): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/upload/image`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: image
		});
	}

	public photoSave(photo): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/photo`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: photo
		});
	}

	public photoUpdate(photo): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/photo/${photo.id}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: photo
		});
	}

	public photoDelete(photoID): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/photo/${photoID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public likesGet(photoID, paging): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/likes/${photoID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: paging
		});
	}

	public likesSave(photoID): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/likes`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: {photo_id: photoID}
		});
	}

	public likesDelete(photoID): Observable<any> {
		return this.apiService.delete({
			endpoint: `${this.configService.xanoApiUrl.value}/likes/${photoID}`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
		});
	}

	public wallGet(external_paging) {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/wall`,
			headers: {Authorization: `Bearer ${this.configService.authToken.value}`},
			params: { external_paging }
		});
	}

}
