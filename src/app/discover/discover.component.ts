import {Component, OnInit} from '@angular/core';
import {PhotoService} from "../photo.service";
import {ViewPanelComponent} from "../view-panel/view-panel.component";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-discover',
	templateUrl: './discover.component.html',
	styleUrls: ['./discover.component.scss']
})
export class DiscoverComponent implements OnInit {

	public photos: any = [];

	constructor(
		private photoService: PhotoService,
		private dialog: MatDialog,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		//todo get route param and do a search against tag
		this.photoService.photosGet(null).subscribe(res => {
			this.photos = res["items"];
		},error =>  {
			this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
				panelClass: 'error-snack'
			})
		})
	}

	public open(photo): void {
		const dialogRef = this.dialog.open(ViewPanelComponent, {data: {photo: photo}, minWidth: '50vw'})

		dialogRef.afterClosed().subscribe(res => {
			if(res?.deleted) {
				this.photos  = this.photos.filter(x => x.id !== res.item.id);
			}
		})
	}
}
