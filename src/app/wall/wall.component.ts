import {Component, OnInit} from '@angular/core';
import {PhotoService} from "../photo.service";
import {ConfigService} from "../config.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-wall',
	templateUrl: './wall.component.html',
	styleUrls: ['./wall.component.scss']
})
export class WallComponent implements OnInit {

	public photos: any = [];
	public hasNextPage: boolean;
	public currentPage: number = 1;

	constructor(
		private configService: ConfigService,
		private photoService: PhotoService,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.configService.newPhoto.subscribe(res => {
			if(res) {
				this.photoService.wallGet(null).subscribe(res => {
					this.photos = res["items"];
					this.currentPage = res["curPage"];
					this.hasNextPage = !!res["nextPage"];
				});
			}
		})
		this.photoService.wallGet(null).subscribe(res => {
			this.photos = res["items"];
			this.currentPage = res["curPage"];
			this.hasNextPage = !!res["nextPage"];
		},error =>  {
			this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
				panelClass: 'error-snack'
			})
		})
	}

	public loadMore() {
		if (this.hasNextPage) {
			this.photoService.wallGet({page: this.currentPage + 1}).subscribe(res => {
				if (res["itemsReceived"] > 0) {
					this.currentPage = res["curPage"];
					this.hasNextPage = !!res["nextPage"]
					this.photos = [...this.photos, ...res["items"]];
				}
			},error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}
}
