import {Component, Input, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {ConfigService} from "../../config.service";
import {PhotoService} from "../../photo.service";
import {MatDialog} from "@angular/material/dialog";
import {LikeListPanelComponent} from "../../like-list-panel/like-list-panel.component";
import {finalize} from "rxjs/operators";
import {ViewPanelComponent} from "../../view-panel/view-panel.component";
import {get} from 'lodash-es';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
	selector: 'app-wall-post',
	templateUrl: './wall-post.component.html',
	styleUrls: ['./wall-post.component.scss']
})
export class WallPostComponent implements OnInit {

	@Input('photo') public photo;
	public commentInput: FormControl = new FormControl('', Validators.required);
	public user: any;

	constructor(
		private configService: ConfigService,
		private photoService: PhotoService,
		private dialog: MatDialog,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.configService.user.asObservable().subscribe(res => this.user = res);
	}

	public toggleLike(photoID): void {
		if (this.photo.liked) {
			this.photoService.likesDelete(photoID).subscribe(res => {
				this.photo.liked = false;
				this.photo.like_count = this.photo.like_count - 1;
			},error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		} else {
			this.photoService.likesSave(photoID).subscribe(res => {
				this.photo.liked = true;
				this.photo.like_count = this.photo.like_count + 1;
			},error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}

	public showLikes(photoID) {
		this.dialog.open(LikeListPanelComponent, {data: {photo_id: photoID}})
	}

	public postComment(photoID) {
		if (this.commentInput.value) {
			this.photoService.commentSave({
				comment: this.commentInput.value,
				photo_id: photoID
			}).toPromise().then(res => {
				const length = this.photo.last_two_comments.items?.length;
				if (length) {
					this.photo.last_two_comments.items.unshift(res);
					if (length > 1) {
						this.photo.last_two_comments.items.pop();
					}
				} else {
					this.photo.last_two_comments.items = [res]
				}
				this.commentInput.patchValue('');
			}).catch(error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}

	public allowDelete(comment, photoOwnerID): boolean {
		return this.user?.id && (comment.user_id === this.user?.id || photoOwnerID === this.user?.id);
	}

	public deleteComment(comment, photoID) {
		this.photoService.commentDelete(comment.id).pipe(
			finalize(() => {
				this.photo.last_two_comments.items = this.photo.last_two_comments.items.filter(y => y.id !== comment.id)
			})).subscribe(res => {}, error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				});
		});
	}

    public open(photo): void {
      this.dialog.open(ViewPanelComponent, {data: {photo: photo}, minWidth: '50vw'})
    }

}
