import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {LikeListPanelComponent} from "../like-list-panel/like-list-panel.component";
import {PhotoService} from "../photo.service";
import {ConfigService} from "../config.service";
import {ManagePanelComponent} from "../manage-panel/manage-panel.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-view-panel',
	templateUrl: './view-panel.component.html',
	styleUrls: ['./view-panel.component.scss']
})
export class ViewPanelComponent implements OnInit {

	public userSelf: any;
	constructor(
		private dialogRef: MatDialogRef<ViewPanelComponent>,
		private configService: ConfigService,
		private photoService: PhotoService,
		@Inject(MAT_DIALOG_DATA) public data,
		private dialog: MatDialog,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		this.userSelf = this.configService.user.value;
	}

	public toggleLike(photoID): void {
		if (this.data.photo.liked) {
			this.photoService.likesDelete(photoID).subscribe(res => {
				this.data.photo.liked = false;
				this.data.photo.like_count  = this.data.photo.like_count -1;
			}, error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		} else {
			this.photoService.likesSave(photoID).subscribe(res => {
				this.data.photo.liked = true;
				this.data.photo.like_count  = this.data.photo.like_count + 1;
			},error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}

	public showLikes(photoID) {
		this.dialog.open(LikeListPanelComponent, {data: {photo_id: photoID}})
	}

	public editPhoto(): void {
		const dialogRef = this.dialog.open(ManagePanelComponent, {data: {photo: this.data.photo}})

		dialogRef.afterClosed().subscribe(res => {
			if(res?.updated) {
				this.data.photo = res.item;
			} else if (res?.deleted) {
				this.dialogRef.close(res);
			}
		})
	}

}
