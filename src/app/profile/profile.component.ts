import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {mergeMap} from "rxjs/operators";
import {UserService} from "../user.service";
import {zip} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';
import {ConfigService} from "../config.service";
import {MatDialog} from "@angular/material/dialog";
import {ViewPanelComponent} from "../view-panel/view-panel.component";
import {FollowsPanelComponent} from "../follows-panel/follows-panel.component";

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	public photos: any;
	public user: any;
	public userSelf: any;
	public showCaptionFor: number;

	constructor(
		private configService: ConfigService,
		private userService: UserService,
		private route: ActivatedRoute,
		private snackBar: MatSnackBar,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.user.asObservable().subscribe(res => {
			this.userSelf = res;
		});

		this.configService.newPhoto.asObservable().subscribe(res => {
			if(res && this.user.id === this.userSelf.id) {
				this.photos.unshift(res);
				this.configService.newPhoto.next(null);
			}
		})

		this.route.params.pipe(mergeMap((params) => {
			return zip(
				this.userService.userGet(params.user_id),
				this.userService.userPhotosGet(params.user_id)
			)
		})).subscribe(result => {
			this.user = result[0];
			this.photos = result[1]['items']
		}, error =>  {
			this.snackBar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
				panelClass: 'error-snack'
			})
		})
	}

	public toggleFollow() {
		if (this.user?.followed) {
			this.userService.userUnfollow(this.user.id).subscribe(res => {
				this.user.followed = false;
			}, error => this.snackBar.open(
				get(error, 'error.message', 'An error occurred'),
				'Error',
				{panelClass: 'error-snack'}
			))
		} else if (this.user) {
			this.userService.userFollow(this.user.id).subscribe(res => {
				this.user.followed = true;
			}, error => this.snackBar.open(
				get(error, 'error.message', 'An error occurred'),
				'Error',
				{panelClass: 'error-snack'}
			))
		}
	}

	public open(photo): void {
		const dialogRef = this.dialog.open(ViewPanelComponent, {data: {photo: photo}, minWidth: '50vw'})

		dialogRef.afterClosed().subscribe(res => {
			if(res?.deleted) {
				this.photos  = this.photos.filter(x => x.id !== res.item.id);
			}
		})
	}

	public openFollowsPanel(userID, following = false) {
		this.dialog.open(FollowsPanelComponent, {data: {user_id: userID, following: following}})
	}
}
