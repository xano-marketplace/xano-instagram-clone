import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from "../config.service";
import { ActivatedRoute } from '@angular/router'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

	public config: XanoConfig;
	public configured: boolean = false;
	public loggedIn: boolean;

	constructor(
		private configService: ConfigService,
		private route: ActivatedRoute
	) {
	}

	ngOnInit(): void {
		this.config = this.configService.config;
		this.route.queryParams.subscribe(res => this.configService.xanoApiUrl.next(res?.api_url));
		this.configService.authToken.asObservable().subscribe(res => {
			this.loggedIn = !!res;
		})
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
	}

}
