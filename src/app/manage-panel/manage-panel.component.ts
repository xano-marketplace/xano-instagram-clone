import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MatChipInputEvent} from "@angular/material/chips";
import {PhotoService} from "../photo.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {finalize, tap} from "rxjs/operators";
import {get} from 'lodash-es';
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import { ConfigService } from 'src/app/config.service'

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {

	readonly separatorKeysCodes: number[] = [ENTER, COMMA];
	public saving: boolean = false;
	public uploading: boolean = false;
	public image: any;
	public photoForm: FormGroup = new FormGroup({
		id: new FormControl(''),
		image: new FormControl('', Validators.required),
		caption: new FormControl(''),
	});

	public tags = new Set();

	constructor(
		private dialogRef: MatDialogRef<ManagePanelComponent>,
		private photoService: PhotoService,
		private configService: ConfigService,
		private snackBar: MatSnackBar,
		@Inject(MAT_DIALOG_DATA) public data,
	) {
	}

	ngOnInit(): void {
		if(this.data?.photo) {
			this.photoForm.patchValue({
				id: this.data.photo.id,
				image: this.data.photo.image,
				caption: this.data.photo.caption
			});
			this.image = this.data.photo.image;
			this.tags = new Set(this.data.photo.tags)
		}
	}

	public addTag(event: MatChipInputEvent): void {
		const input = event.input;
		const value = event.value;

		if ((value || '').trim()) {
			this.tags.add(value.trim());
		}
		if (input) {
			input.value = '';
		}
	}

	public removeTag(tag): void {
		this.tags.delete(tag);
	}

	public upload(event): void {
		this.uploading = true;
		const file: File = event.target.files[0];
		const formData: FormData = new FormData();
		formData.append('content', file, file.name);
		this.photoService.photoUpload(formData)
			.pipe(
				finalize(() => this.uploading = false)
			)
			.subscribe(res => {
				this.photoForm.controls.image.patchValue(res);
				const reader = new FileReader();
				reader.onload = () => this.image = reader.result;
				reader.readAsDataURL(file);
			}, error => this.snackBar.open('Image Upload', 'Failed', {panelClass: 'error-snack'}));
	}

	public submit(): void {
		this.photoForm.markAllAsTouched();
		if (this.photoForm.valid) {
			if (!this.data?.photo) {
				this.photoService.photoSave({
					...this.photoForm?.value,
					tags: [...this.tags],
				}).pipe(
					tap(x => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({new: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			} else {
				this.photoService.photoUpdate({
					...this.photoForm?.value,
					tags: [...this.tags],
				}).pipe(
					tap(() => this.saving = true),
					finalize(() => this.saving = false)
				).subscribe((res: any) => {
					this.dialogRef.close({updated: true, item: res});
				}, err => {
					this.snackBar.open(get(err, 'error.message', 'An error has occurred'), 'Error', {panelClass: 'error-snack'});
				});
			}
		} else {
			if (!this.configService.user.value?.id) {
				this.snackBar.open('Please Login', 'Error', {panelClass: 'error-snack'});
			} else {
				this.snackBar.open('Please Fix Form', 'Error', {panelClass: 'error-snack'});
			}
		}
	}

	public deletePhoto(): void {
		if(this.data?.photo) {
			this.photoService.photoDelete(this.data.photo.id)
				.pipe(finalize(()=> this.snackBar.open('Photo', 'Deleted')))
				.subscribe(res=> {
					this.dialogRef.close({deleted: true, item: this.data.photo});
				}, error => this.snackBar.open(
					get(error, 'error.message', 'An error has occurred'),
					'Error',
					{panelClass: 'error-snack'}))
		}
	}
}
