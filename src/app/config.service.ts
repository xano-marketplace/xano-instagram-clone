import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public authToken: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public user: BehaviorSubject<any> = new BehaviorSubject<any>(null);
	public newPhoto: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public config: XanoConfig = {
		title: 'Photo-Sharing Social Media platform',
		summary: 'A photo-sharing social media platform similar to Instagram.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-instagram-clone',
		descriptionHtml: `
                <h2>Description</h2>
                <p class="pl-4 pr-4">
					This demo uses a mock front-end to illustrate the use of the photo-sharing social media template. 
					Some of the features it includes are photo-sharing and posting, liking, commenting, and following. 
					Additionally, a user profile and wall are included. Each of the API endpoints requires authentication.
                </p>
              	<h2>Components</h2>
              	<ul>
              		<li>
						<b>Action Bar</b>
						<p class="pr-4">
							This is where basic navigation happens; you can move from the wall to the discover page,
							 post a new photo, and click on your photo or handle (username) to go to your profile.
							 You can also search in the Action Bar, which allows you to search users by name or handle. 
							 Additionally, the login and signup buttons live here.
						</p>
					</li>
					<li>
						<b>Wall</b>
						<p class="pr-4">
							The wall is where you will see all of your shared photos and those of other users you follow 
							in chronological order. You can like and comment directly on the wall or open a photo in a side 
							panel to view all comment
						</p>
					</li>
					<li>
						<b>Discover</b>
						<p class="pr-4">
							Discover replicates the basic functionality of Instagram's Explore; it grabs the latest 
							photos that have been shared on the platform. While not included in this demo, 
							the endpoint allows for search where you could extend with algorithms or use search by tag, 
							descriptions, etc
						</p>
					</li>
					<li>
              			<b>Profile</b>
              			<p class="pr-4">
							The profile displays all photos that a user has shared; you can like and comment on a 
							photo by opening it in the side panel view.
              			</p>
					</li>
					<li>
						<b>Panels</b>
						<p class="pr-4">
							There are multiple side panels in this demo. 
							<ul>
								<li>The Auth panel allows users to log in and sign up to the platform.</li>
								<li>The view panel shows a close-up view of a photo and includes an edit icon if the user owns the posted photo.</li>
								<li>
									The manage panel enables you to change the description and tags of an existing photo, delete an existing photo, or post a new photo.
								</li>
								<li>The like panel provides a list of the likes of a photo.</li>
								<li>The follow panel lists all the user followers and who the user is following.</li>
							</ul>
						</p>
					</li>
				</ul>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/user',
			'/photo',
			'/comments',
			'/likes',
			'/follows'
		]
	};

	constructor(private apiService: ApiService, private xanoService: XanoService) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public isLoggedIn(): Observable<any> {
		return this.authToken.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
