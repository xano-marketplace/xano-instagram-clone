import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HeaderComponent} from './header/header.component';
import {HomeComponent} from './home/home.component';
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ActionBarComponent} from './action-bar/action-bar.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatButtonModule} from "@angular/material/button";
import {ConfigPanelComponent} from './config-panel/config-panel.component';
import {AuthPanelComponent} from './auth-panel/auth-panel.component';
import {ManagePanelComponent} from './manage-panel/manage-panel.component';
import {SafeHtmlPipe} from "./safe-html.pipe";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatIconModule} from "@angular/material/icon";
import {MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule} from "@angular/material/snack-bar";
import {MatCommonModule} from "@angular/material/core";
import {ProfileComponent} from './profile/profile.component';
import {WallComponent} from './wall/wall.component';
import {CommentsComponent} from './comments/comments.component';
import {MatChipsModule} from "@angular/material/chips";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {DiscoverComponent} from './discover/discover.component';
import {ViewPanelComponent} from './view-panel/view-panel.component';
import {LikeListPanelComponent} from './like-list-panel/like-list-panel.component';
import {MatListModule} from "@angular/material/list";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import {MomentModule} from "ngx-moment";
import {MatMenuModule} from "@angular/material/menu";
import {InfiniteScrollModule} from "ngx-infinite-scroll";
import { WallPostComponent } from './wall/wall-post/wall-post.component';
import { FollowsPanelComponent } from './follows-panel/follows-panel.component';

@NgModule({
	declarations: [
		AppComponent,
		HomeComponent,
		HeaderComponent,
		ActionBarComponent,
		ConfigPanelComponent,
		AuthPanelComponent,
		ManagePanelComponent,
		SafeHtmlPipe,
		ProfileComponent,
		WallComponent,
		CommentsComponent,
		DiscoverComponent,
		ViewPanelComponent,
		LikeListPanelComponent,
		WallPostComponent,
		FollowsPanelComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatCommonModule,
		HttpClientModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		MatToolbarModule,
		MatIconModule,
		MatButtonModule,
		MatSnackBarModule,
		MatCardModule,
		MatInputModule,
		MatDialogModule,
		MatChipsModule,
		MatProgressSpinnerModule,
		MatListModule,
		MatAutocompleteModule,
		MomentModule,
		MatMenuModule,
		InfiniteScrollModule
	],
	providers: [{
		provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
		useValue: {
			duration: 4000,
			horizontalPosition: 'start'
		}
	}, {
		provide: MAT_DIALOG_DEFAULT_OPTIONS,
		useValue: {
			hasBackdrop: true,
			minHeight: '100vh',
			position: {top: '0px', right: '0px'},
			panelClass: 'slide-out-panel'
		}
	}],
	bootstrap: [AppComponent]
})
export class AppModule {
}
