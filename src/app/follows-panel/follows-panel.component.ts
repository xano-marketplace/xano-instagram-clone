import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {UserService} from "../user.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';

@Component({
	selector: 'app-follows-panel',
	templateUrl: './follows-panel.component.html',
	styleUrls: ['./follows-panel.component.scss']
})
export class FollowsPanelComponent implements OnInit {
	public follows: any = [];

	constructor(
		private userService: UserService,
		@Inject(MAT_DIALOG_DATA) public data,
		private snackbar: MatSnackBar
	) {
	}

	ngOnInit(): void {
		if (this.data?.user_id) {
	        this.userService.userFollowsGet(this.data.user_id, this.data.following)
			.subscribe(res => {
	            this.follows  = res.items;
            },error =>  {
				this.snackbar.open(get(error, 'error.message', 'An Error Occurred'), 'Error', {
					panelClass: 'error-snack'
				})
			});
		}
	}
}
